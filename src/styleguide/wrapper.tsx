import React from 'react';

import { ThemeProvider } from '@emotion/react';
import { themes } from '../theme'

const Wrapper: React.FC = ({ children }) => (
    <ThemeProvider theme={themes.lightTheme}>{children}</ThemeProvider>
);

export default Wrapper;
