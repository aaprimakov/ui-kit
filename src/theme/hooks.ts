import { useState } from 'react';
import { nextTheme, themesArray } from '.';

export const useTheme = () => {
    const [[themeName, theme], setTheme] = useState(themesArray[0]);

    const handleNextThemeClick = () => {
        setTheme(nextTheme(themeName));
    };

    return {
        theme,
        handleNextThemeClick,
    }
}
