const commonSizes = {
    sm: 4,
    sm2: 8,
    md0: 12,
    md: 16,
    md2: 24,
    lg: 32,
    lg2: 64,
};

const lightTheme = {
    sizes: commonSizes,
    colors: {
        primary: {
            main: '#9B51E0',
        },
        action: {
            main: '#85D97E',
            'main.bright': '#92FA89',
            'main.disabled': '#89A687',
        },
        gray: {
            smooth: '#fbfbfb',
            light: '#F5F5F5',
            light2: '#D4D4D8',
        },
        white: '#ffffff',
        black: '#000000',
        error: {
            bg: '#de292994',
            border: '#8400004f',
            text: '#ffffff',
        },
        warning: '#FF8C05',
        noColor: 'rgba(0,0,0,0)',
    },
    shadows: {
        lvl1: '3px 8px 17px rgba(0, 0, 0, 0.09)',
        lvl1_hover: '3px 8px 20px 0px #00000030',
        lvl2: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    },
};

export type AppTheme = typeof lightTheme;
export type WithTheme = { theme?: AppTheme };

const darkTheme: AppTheme = {
    sizes: commonSizes,
    colors: {
        primary: {
            main: '#9B51E0',
        },
        action: {
            main: 'blue',
            'main.bright': '#92FA89',
            'main.disabled': '#89A687',
        },
        gray: {
            smooth: '#fbfbfb',
            light: '#F5F5F5',
            light2: '#D4D4D8',
        },
        white: '#ffffff',
        black: '#000000',
        error: {
            bg: '#de2929d1',
            border: '#840000',
            text: '#ffffff',
        },
        warning: '#FF8C05',
        noColor: 'rgba(0,0,0,0)',
    },
    shadows: {
        lvl1: '3px 8px 17px rgba(0, 0, 0, 0.09)',
        lvl1_hover: 'box-shadow: 3px 8px 20px 0px #00000030',
        lvl2: '0px 4px 4px rgba(0, 0, 0, 0.25)',
    },
};

enum Themes {
    lightTheme = 'lightTheme',
    darkTheme = 'darkTheme',
}

export const themesArray: [Themes, AppTheme][] = [
    [Themes.lightTheme, lightTheme],
    [Themes.darkTheme, darkTheme],
];

export const themes = {
    [Themes.lightTheme]: lightTheme,
    [Themes.darkTheme]: darkTheme,
}

export const nextTheme = (currentTheme: Themes): typeof themesArray[0] => {
    const currentIndex = themesArray.findIndex(t => (t[0] = currentTheme));

    if (currentIndex === themesArray.length) {
        return themesArray[0];
    }

    return themesArray[currentIndex + 1];
};
