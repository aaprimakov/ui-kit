import React from 'react';

import styled from '@emotion/styled';
import { WithTheme } from '../../theme';

type WrapperProps = {
    size: number;
} & WithTheme;

export const Wrapper = styled.div<WrapperProps>`
    width: 100%;
    font-size: ${({ size }) => size}em;
    padding: ${({ theme: { sizes } }) => sizes.md}px;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const Rail = styled.div<WithTheme>`
    width: 16em;
    height: 8em;
    position: relative;
    overflow: hidden;

    &::before,
    &::after {
        content: '';
        position: absolute;
        bottom: 0;
    }

    &::before {
        width: inherit;
        height: 0.2em;
        background-color: ${({ theme: { colors } }) => colors.primary.main};
        opacity: 0.3;
    }

    &::after {
        box-sizing: border-box;
        width: 50%;
        height: inherit;
        border: 0.2em solid ${({ theme: { colors } }) => colors.primary.main};
        opacity: 0.3;
        border-radius: 50%;
        left: 25%;
    }
`;

export const Sphere = styled.span<WithTheme>`
    position: absolute;
    width: 5%;
    height: 10%;
    background-color: ${({ theme: { colors } }) => colors.primary.main};
    border-radius: 50%;
    bottom: 0.2em;
    left: -5%;
    animation: 2s linear infinite;
    transform-origin: 50% -3em;
    animation-name: run, rotating;

    :nth-child(2) {
        animation-delay: 0.075s;
    }
    :nth-child(3) {
        animation-delay: 0.15s;
    }

    @keyframes run {
        0% {
            left: -5%;
        }
        10%,
        60% {
            left: calc((100% - 5%) / 2);
        }
        70%,
        100% {
            left: 100%;
        }
    }

    @keyframes rotating {
        0%,
        10% {
            transform: rotate(0deg);
        }
        60%,
        100% {
            transform: rotate(-1turn);
        }
    }
`;

