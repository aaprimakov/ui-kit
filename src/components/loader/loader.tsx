import React from "react";

import { themes, WithTheme } from '../../theme';

import { Wrapper, Rail, Sphere } from "./style";

type LoaderProps = {
    /** Размер */
    size?: number;
} & WithTheme;

export const Loader: React.FC<LoaderProps> = ({ size }) => (
    <Wrapper size={size}>
        <Rail>
            <Sphere/>
            <Sphere/>
            <Sphere/>
        </Rail>
    </Wrapper>
);

Loader.defaultProps = {
    /** Размер */
    size: 0.3,
};
