import styled from '@emotion/styled';

import { WithTheme } from '../../theme';

export const ButtonStyle = styled.button<WithTheme>`
    background-color: ${({ theme: { colors } }) => colors.action.main};
    color: ${({ theme: { colors } }) => colors.white};
    padding: ${({ theme: { sizes } }) => `${sizes.md}px ${sizes.lg2}px`};
    transition: 0.3s linear box-shadow;
    border: none;
    border-radius: 4px;
    font-size: 14px;
    font-weight: 400;
    max-width: max-content;

    ${({ disabled, theme: { colors } }) =>
        disabled ? `background-color: ${colors.action['main.disabled']};` : ''}
    ${({ disabled }) => (!disabled ? `cursor: pointer;` : '')}
    
    :hover {
        ${({ theme: { shadows }, disabled }) =>
            !disabled ? `box-shadow: ${shadows.lvl1};` : ''}
        background-color: ${({ theme: { colors } }) =>
            colors.action['main.bright']};
        ${({ disabled, theme: { colors } }) =>
            disabled
                ? `background-color: ${colors.action['main.disabled']}`
                : ''}
    }
`;
