import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

import { WithTheme, themes } from '../../theme';
import { Loader } from '../loader';

import { ButtonStyle } from './styled';

type ButtonProps = {
    loading?: boolean;
} & WithTheme & React.ButtonHTMLAttributes<HTMLButtonElement>;

export const Button: React.FC<ButtonProps> = ({
    loading,
    children,
    ...rest
}) => (
    <ButtonStyle {...rest}>
        {loading ? <Loader size={0.2} /> : children}
    </ButtonStyle>
);

Button.propTypes = {
    /** Признак загрузки (отображаем лоадер) */
    loading: PropTypes.bool,
}

Button.defaultProps = {
    loading: false,
}


