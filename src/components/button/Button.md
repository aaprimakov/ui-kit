Button example:

## Просто кнопка

```js
<Button>🍕</Button>
```

## Кнопка с loading=true

```js
<Button loading={true}>🍕</Button>
```
