Button example:

## Просто кнопка

```js
<ButtonIcon>🍕</ButtonIcon>
```

## Кнопка с loading=true

```js
<ButtonIcon loading={true}>🍕</ButtonIcon>
```
