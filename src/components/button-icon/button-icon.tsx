import React from 'react';
import styled from '@emotion/styled';

import { WithTheme } from '../../theme';

import { Button } from '../button';

export const ButtonIcon = styled(Button)<WithTheme>`
    padding: ${({ theme: { sizes } }) => `6px ${sizes.md0}px`};
    display: flex;
    align-items: center;
    justify-content: center;
`;
