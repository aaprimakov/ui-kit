const path = require('path');

module.exports = {
    components: 'src/components/**/[a-z-]*.tsx',
    // propsParser: require("react-docgen-typescript").withDefaultConfig().parse,
    styleguideComponents: {
        Wrapper: path.join(__dirname, 'src/styleguide/wrapper')
    }
}
